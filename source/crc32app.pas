program crc32app;

{

     2017 Jerome Shidel

     Released as Public Domain.

     Do with it as you will. :-)

}

//  Requires The Free Pascal Complier

uses CRC;

var
   Index : LongInt;
   CRC_Value : Cardinal;
   FileName : String;
   Buffer : array[0..1023] of byte;


function Hex(Value : Cardinal): String;
const
  Digits : String[16] = '0123456789abcdef';
var
   Temp : String;
begin
   Temp := '';
   while Value > 0 do begin
      Temp := Digits[Value and $f + 1] + Temp;
      Value := Value shr 4;
   end;
   while Length(Temp) < 8 do
      Temp := '0' + Temp;
   Result := Temp;
end;

procedure ShowResult;
begin
  if ParamCount > 1 then begin
     WriteLn(Hex(CRC_Value), #9, FileName)
  end else begin
     WriteLn(Hex(CRC_Value));
  end;
end;

procedure Do_CRC;
var
   F : File;
   C : LongInt;
begin
   CRC_Value := crc.crc32(0, nil, 0);
   {$I-}
   Assign(F, FileName);
   Reset(F, 1);
   if IOResult = 0 then begin
       C := SizeOf(Buffer);
       while (C = SizeOf(Buffer)) and (IOResult = 0) do begin
         BlockRead(F, Buffer, SizeOf(Buffer), C);
         if C > 0 then begin
            CRC_Value := crc.crc32(CRC_Value, Buffer, C);
         end;
       end;
       Close(F);
   end else
       CRC_Value := 0;
   {$I+}
   ShowResult;
end;

{$R *.res}

begin
  if ParamCount > 0 then begin
     Index := 0;
     while Index < ParamCount do begin
        Inc(Index);
        FileName := ParamStr(Index);
        Do_CRC;
     end
  end
end.

