# crc32

simple FPC program to create CRC checksums.

2017 Jerome Shidel, The Unlicense (Public Domain)

Most "modern" operating systems no longer provide a utility to create or
verify CRC32 checksums. This extremely simple Free Pascal program is used
by environments like the RBE (Release Build Environment) when generating
metadata and package lists for DOS programs.
